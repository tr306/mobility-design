//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef AUVMOBILITYPROJECT_AUVMOBILITY_H_
#define AUVMOBILITYPROJECT_AUVMOBILITY_H_


#include "TraceMobilityProjectTemplate/TraceMobility.h"


class AUVMobility: public TraceMobility {
    public:
        virtual void initialize(int stage) override;
        virtual void handleSelfMessage(cMessage *msg) override;
        virtual void scheduleNextWaypoint() override;

    private:
        // virtual for future better implementation
        virtual void calcCurve();
        virtual void transform();
        virtual void calcRightCurve();
        virtual void calcLeftCurve();
        virtual double calcTangentialXRight(double xDestination, double yDestination);
        virtual double calcTangentialXLeft(double xDestination, double yDestination);
        virtual void reverseTransformation();
        virtual void addWaypointsAndAdjust(double last[3], simtime_t lastTime);
        virtual bool checkCurrentDirection();
        virtual bool checkIfGoalIsBelowCircle();


        int interpolationStepCounter;
        int nonInterpolatedWaypoints;

        bool firstInterpolation;

        double pos[3]; //position
        double dir[3]; //direction
        double goal[3];
        double last[3];//last interpolated Waypoint

        //variables for inside the transformation
        double newXAxis [2],
               newYAxis [2],
               newOrigin[2],

               newGoal [2],
               newPos [2];

        waypointEventsList curvePoints;
};

#endif /* AUVMOBILITYPROJECT_AUVMOBILITY_H_ */
