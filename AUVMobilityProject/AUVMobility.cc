//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "AUVMobility.h"
#include <math.h>
#include <algorithm>

#define CURVATURE_INTERPOLATION_STEPS 10

#define CORNERSPEED 1.0

#define Z_PLAIN 10

using namespace omnetpp;

Define_Module(AUVMobility);

void AUVMobility::initialize(int stage) {
    interpolationStepCounter = 0;

    last[0] = 0.0;
    last[1] = 0.0;
    last[2] = 0.0;

    dir[0] = 0;
    dir[1] = 1;
    dir[2] = 0;

    pos[0] = 0;
    pos[1] = 0;
    pos[2] = 0;

    firstInterpolation = true;

    TraceMobility::initialize(stage);
}

void AUVMobility::handleSelfMessage(cMessage *msg) {
    TraceMobility::handleSelfMessage(msg);
}

void AUVMobility::scheduleNextWaypoint() {

    if(firstInterpolation) {
        nonInterpolatedWaypoints = eventList.size();
        interpolationStepCounter = nonInterpolatedWaypoints;
        firstInterpolation = false;
    }

    goal[0] = eventList.front().getX();
    goal[1] = eventList.front().getY();
    goal[2] = eventList.front().getZ();


    if (interpolationStepCounter == nonInterpolatedWaypoints && !eventList.empty()) {
        nonInterpolatedWaypoints = eventList.size();
        if (!checkCurrentDirection()) {
            calcCurve();
        }
    } else {
        interpolationStepCounter--;
    }

    dir[0] = goal[0] - pos[0];
    dir[1] = goal[1] - pos[1];
    dir[2] = goal[2] - pos[2];

    pos[0] = eventList.front().getX();
    pos[1] = eventList.front().getY();
    pos[2] = eventList.front().getZ();

    TraceMobility::scheduleNextWaypoint();
}

void AUVMobility::calcCurve() {
    transform();

    if(checkIfGoalIsBelowCircle()) return;

    if (newGoal[0] > 0) {
        calcRightCurve();
    } else {
        calcLeftCurve();
    }
    reverseTransformation();

    addWaypointsAndAdjust(last, curvePoints.front().getTimeAtDest());

    curvePoints.clear();

    interpolationStepCounter = eventList.size();
}

void AUVMobility::transform() {
    double length = sqrt(dir[0]*dir[0] + dir[1]*dir[1]);

    newYAxis[0] = dir[0]/length;
    newYAxis[1] = dir[1]/length;

    newXAxis[0] =  newYAxis[1];
    newXAxis[1] = -newYAxis[0];

    newOrigin[0] = pos[0];
    newOrigin[1] = pos[1];

    // Avoidance of zero-division
    if (newYAxis[0] == 0.0) {
        if (newYAxis[1] > 0.0) {            // Y' = (0,1)
            newGoal[0] = goal[0] - newOrigin[0];
            newGoal[1] = goal[1] - newOrigin[1];
        } else {                            // Y' = (0,-1)
            newGoal[0] =  - goal[0] + newOrigin[0];
            newGoal[1] =  - goal[1] + newOrigin[1];
        }
    } else {
        if (newYAxis[1] == 0.0) {
            if (newYAxis[0] > 0.0) {        // Y' = (1,0)
                newGoal[0] = -goal[1] + newOrigin[1];
                newGoal[1] =  goal[0] - newOrigin[0];
            } else {                        //Y' = (-1,0)
                newGoal[0] = goal[1] - newOrigin[1];
                newGoal[1] = -goal[0] + newOrigin[0];
            }
        } else {
            newGoal[1] = ((goal[1]-newOrigin[1])*newXAxis[0]-(goal[0]-newOrigin[0])*newXAxis[1]) / (newYAxis[1]*newXAxis[0] - newYAxis[0]*newXAxis[1]);
            newGoal[0] = ((goal[0]-newOrigin[0])*newXAxis[1]-(newYAxis[0]*newXAxis[1])*newGoal[1]) / (newXAxis[0] * newXAxis[1]);
        }
    }

}

//returns true, if AUV is already going in the correct direction
//side effect: also returns true if a 180°-rotation is necessary
bool AUVMobility::checkCurrentDirection () {
    if (dir[0] == 0 && goal[0] == pos[0]) {
        return true;
    }

    //goal[0] == pos[0] + scale * dir[0]
    double scale = (goal[0]-pos[0])/dir[0];
    return (goal[1] == pos[1] + scale * dir[1]);
}

//returns true if the new goal is below the
bool AUVMobility::checkIfGoalIsBelowCircle() {
    if (newGoal[0] <= 3.0 && newGoal[0] >= -3.0) {
        if ((newGoal[0] > 0 && newGoal[1] <= sqrt((6 * newGoal[0]) - (newGoal[0] * newGoal[0]))) || (newGoal[0] > 0 && newGoal[1] <= sqrt(-(6 * newGoal[0]) - (newGoal[0] * newGoal[0])))) return true;
    }
    return false;
}

//approximates the x-Value on the circle function, for which the is the same as the incline of the linear
//function that connects the destination point with the current point on the circle function
double AUVMobility::calcTangentialXRight(double xDestination, double yDestination) {
    double x = 0.01, y = 0.0, lastX = 0.0;
    for (; x < 6.0; x += 0.01) {
        y = sqrt(6 * x - x * x);
        if ((((yDestination - y)/(xDestination-x)) - ((3-x)/y)) >= 0.0) {
            break;
        } else {
            lastX = x;
        }
    }

    return lastX;
}

//for left curves
double AUVMobility::calcTangentialXLeft(double xDestination, double yDestination) {
    double x = -0.01, y = 0.0, lastX = 0.0;
    for (; x > -6.0; x -= 0.01) {
        y = sqrt(-6 * x - x * x);

        if ((((yDestination - y)/(xDestination-x)) - ((-3-x)/y)) <= 0.0) {
            break;
        } else {
            lastX = x;
        }
    }

    return lastX;
}

void AUVMobility::calcRightCurve() {
    SetDestEv nextCurvePosition;
    double tanX = 0.0,
           interval = 0.0,
           distance = 0.0;

    double next[3];
    next[0] = 0.0;
    next[1] = 0.0;
    next[2] = Z_PLAIN;

    last[0] = 0.0;
    last[1] = 0.0;
    last[2] = 0.0;

    simtime_t lastTime = simTime();

    tanX = calcTangentialXRight(newGoal[0], newGoal[1]);
    interval = tanX / CURVATURE_INTERPOLATION_STEPS;

    for (double i = interval; i < tanX; i += interval) {
        //SetDestEv with next waypoint & times
        //y = sqrt(6*x - x*x)

        next[0] = i;
        next[1] = sqrt((6 * i) - (i * i));
        distance = sqrt(pow(next[0] - last[0], 2) + pow(next[1] - last[1], 2));

        last[0] = next[0];
        last[1] = next[1];
        last[2] = next[2];

        nextCurvePosition.setTime(lastTime);
        nextCurvePosition.setX(next[0]);
        nextCurvePosition.setY(next[1]);
        nextCurvePosition.setZ(next[2]);
        nextCurvePosition.setSpeed(CORNERSPEED);

        //time = distance/speed
        lastTime = SimTime(lastTime.dbl() + (distance / CORNERSPEED));

        nextCurvePosition.setTimeAtDest(lastTime);

        curvePoints.push_front(nextCurvePosition);
    }
}

void AUVMobility::calcLeftCurve() {
    SetDestEv nextCurvePosition;
    double tanX = 0.0,
           interval = 0.0,
           distance = 0.0;

    double next[3];
           next[0] = 0.0;
           next[1] = 0.0;
           next[2] = Z_PLAIN;

    last[0] = 0.0;
    last[1] = 0.0;

    simtime_t lastTime = simTime();

    tanX = calcTangentialXLeft(newGoal[0], newGoal[1]);
    interval = tanX / CURVATURE_INTERPOLATION_STEPS;

    for (double i = interval; i > tanX; i += interval) {
       //SetDestEv with next waypoint & times
       //y = sqrt(- 6*x - x*x)

       next[0] = i;
       next[1] = sqrt(- 6 * i - i * i);
       distance = sqrt(pow(next[0] - last[0], 2) + pow(next[1] - last[1], 2));

       last[0] = next[0];
       last[1] = next[1];

       nextCurvePosition.setTime(lastTime);
       nextCurvePosition.setX(next[0]);
       nextCurvePosition.setY(next[1]);
       nextCurvePosition.setZ(next[2]);
       nextCurvePosition.setSpeed(CORNERSPEED);

       //time = distance/speed
       lastTime = SimTime(lastTime.dbl() + distance / CORNERSPEED);

       nextCurvePosition.setTimeAtDest(lastTime);

       curvePoints.push_front(nextCurvePosition);

   }
}

void AUVMobility::reverseTransformation() {
    std::list<SetDestEv>::iterator i;

    double currentGoal[2];

    for (i = curvePoints.begin(); i != curvePoints.end(); ++i) {
        currentGoal [0] = i->getX();
        currentGoal [1] = i->getY();

        i->setX(pos[0] + currentGoal[0] * newXAxis[0] + currentGoal[1] * newYAxis[0]);
        i->setY(pos[1] + currentGoal[0] * newXAxis[1] + currentGoal[1] * newYAxis[1]);

    }
}

void AUVMobility::addWaypointsAndAdjust(double last[], simtime_t lastTime) {
    //Fill the next waypoint seperately, adjust travel time according to new distance traveled
    last[0] = curvePoints.front().getX();
    last[1] = curvePoints.front().getY();
    last[2] = curvePoints.front().getZ();

    double nextX = eventList.front().getX(),
           nextY = eventList.front().getY(),
           nextZ = eventList.front().getZ(),

           distance = sqrt(pow(nextX - last[0], 2) + pow(nextY - last[1], 2) + pow(nextZ - last[2], 2)),
           timeDifference;

    eventList.front().setTime(lastTime);
    eventList.front().setTimeAtDest(lastTime + distance / eventList.front().getSpeed());

    curvePoints.push_front(eventList.front());
    eventList.pop_front();

    //fill leftover wayPoints into Curvepoints and adjust for time
    while (!eventList.empty()) {
        timeDifference = eventList.front().getTimeAtDest().dbl()
                - eventList.front().getTime().dbl();
        eventList.front().setTime(curvePoints.front().getTimeAtDest());
        eventList.front().setTimeAtDest(
                SimTime(eventList.front().getTime().dbl() + timeDifference));

        curvePoints.push_front(eventList.front());
        eventList.pop_front();
    }

    //copy curvePoints into eventList (in reverse)
    eventList.resize(curvePoints.size());
    std::reverse_copy(curvePoints.begin(), curvePoints.end(),eventList.begin());
}

/*
 * Example code to test what is in eventListList at any place in the code
    waypointEventsList tmpEventList;
    tmpEventList.resize(10);
    std::copy(eventList.begin(), eventList.end(),tmpEventList.begin());

 */

