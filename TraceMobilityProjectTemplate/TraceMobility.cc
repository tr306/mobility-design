// ***************************************************************************
// 
// OppoNet Project
// This file is a part of the opponet project, jointly managed by the
// Laboratory for Communications Networks (LCN) at KTH in Stockholm, Sweden
// and Reykjavik University, Iceland.
//
// Copyright (C) 2008 Olafur Ragnar Helgason, Laboratory for Communication 
//                    Networks, KTH, Stockholm
//           (C)      Kristjan Valur Jonsson, Reykjavik University, Reykjavik
//
// ***************************************************************************
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License version 3
// as published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not see <http://www.gnu.org/licenses/>.
//
// ***************************************************************************

#include "../TraceMobilityProjectTemplate/TraceMobility.h"

Define_Module(TraceMobility);

void TraceMobility::initialize(int stage)
{
    MovingMobilityBase::initialize(stage); // Modified Peter

    startPos = lastPosition; // Added Peter
    startTime = simTime(); // Added Peter
    direction = Coord::ZERO; // Added Peter
    speed = 0;


    if (stage == 0) {
        nodeId = par("nodeId").longValue();
        if (nodeId == -1) {
            nodeId = this->getParentModule()->getId();
        }
        moving = false;
        interpolate = par("interpolate").boolValue();
        if (interpolate) {
            updateInterval = par("updateInterval");
        }
        updateEvent = new cMessage("updateEvent");
        // we want move-update events to have a higher priority than any other
        // events scheduled simultaneously.  This ensures that position is always
        // updated before other events.
        updateEvent->setSchedulingPriority(-1);
    } else if (stage == 1) {
        // Peter ev << "TraceMobility initialized at "<< move.getStartPos().info() << endl;
        EV << "TraceMobility initialized at "<< startPos << endl; // Added Peter
    }
}

void TraceMobility::finish()
{
    MovingMobilityBase::finish(); // Modified Peter
    emitMobilityStateChangedSignal();
    cancelAndDelete(updateEvent);
}

// Peter void TraceMobility::handleSelfMsg(cMessage* msg)
void TraceMobility::handleSelfMessage(cMessage *msg) // Added Peter
{
   if (msg == updateEvent) { // Modified Peter
        if(moving) {
            // Peter makeMove();
            // Peter updatePosition();
            moveAndUpdate(); // Added Peter
        } else {
            startMoving();
        }
    } else {
        MovingMobilityBase::handleSelfMessage(msg); // Modified Peter
    }
}

// Peter void TraceMobility::makeMove()
void TraceMobility::move() // Added Peter
{
    Enter_Method_Silent();
    if (moving) // Added Peter
    {
        step++;
        if (step == numSteps) {
            // Destination reached
            // Peter move.setStart(targetPos);
            startPos = targetPos; // Added Peter
            lastPosition = startPos;
            startTime = simTime(); // Added Peter
            // Peter move.setSpeed(0);
            speed = 0; // Added Peter
            // Peter move.setDirectionByVector(Coord(0,0));
            direction = Coord(0,0,0);                                   // Uli: z direction added
            // Peter ev << "Target " << targetPos.info()
            EV << "Target " << targetPos // Added Peter
                       << " reached, scheduling next waypoint" << endl;
            moving = false;

            scheduleNextWaypoint();
        } else if (step < numSteps) {
            // step forward
            // Peter move.setStart(stepTarget);
            startPos = lastPosition; // Added Peter
            startTime = simTime(); // Added Peter

            lastPosition += stepSize;
            if (updateEvent->isScheduled()) {
                cancelEvent(updateEvent);
            }

            if (numSteps - step == 1) {
                scheduleAt(timeAtTarget, updateEvent);
            } else {
                scheduleAt(simTime() + updateInterval, updateEvent);
            }
        } else {
            throw cRuntimeError("step > numSteps in TraceMobility");
        }
    }
}

void TraceMobility::scheduleNextWaypoint()
{
    if (!eventList.empty()) {
        SetDestEv& waypoint = eventList.front();

        if(updateEvent->isScheduled()) {
            //throw cRuntimeError("Update event is already scheduled");
            cancelEvent(updateEvent);
        }
        scheduleAt(waypoint.getTime(), updateEvent);


    }
}

void TraceMobility::startMoving()
{
    if (eventList.empty()) {
        return;
        //throw cRuntimeError("No mobility events available!");
    }
    SetDestEv waypoint = eventList.front();
    eventList.pop_front();
    simtime_t now = simTime();
    if (waypoint.getTime() != now) {
        throw cRuntimeError("waypoint.getTime() != now");
    }
    if (waypoint.getTimeAtDest() < now) {
        throw cRuntimeError("timeAtDest (%s) is in the past (now = %s)",
                waypoint.getTimeAtDest().str().c_str(), now.str().c_str());
    }
    // TODO implement 3D support
    targetPos = Coord(waypoint.getX(), waypoint.getY(), waypoint.getZ());   // Uli: z coordinate added
    timeAtTarget = waypoint.getTimeAtDest();
    step = 0;
    // Peter if (targetPos == move.getStartPos()) {
    if (targetPos == startPos) { // Added Peter
        numSteps = 1;
        speed = 0;
        moving = false;
        scheduleNextWaypoint();
    } else {
        // Peter Coord pos = move.getStartPos();
        Coord pos = startPos; // Added Peter
        speed = pos.distance(targetPos) / (timeAtTarget - now);
        //if( (waypoint->getSpeed() - _speed)*(waypoint->getSpeed() - _speed) > 0.01 ) {
        //    ev << "waypoint->getSpeed()=" << waypoint->getSpeed() 
        //          << ", _speed=" << _speed << endl;
        //    throw cRuntimeError("Speed mismatch in tracefile");
        //}
        // Peter move.setSpeed(speed);
        // Peter move.setDirectionByTarget(targetPos);
        direction = targetPos - startPos; // Added Peter
        moving = true;
        // calculate steps for interpolate or non-interpolate
        simtime_t nextEventTime;
        if (!interpolate) {
            numSteps = 1;
            nextEventTime = timeAtTarget;
        } else {
            // Get the number of steps needed to be covered.
            simtime_t travelTime = timeAtTarget - now;
            numSteps = static_cast < int >(ceil(travelTime.dbl() / updateInterval));
            if (numSteps > 1) {
                stepSize = direction / numSteps;
                // Peter stepTarget = move.getStartPos() + stepSize;
                nextEventTime = now + updateInterval;
            } else {
                nextEventTime = timeAtTarget;
            }
        }
        if (updateEvent->isScheduled()) {
            cancelEvent(updateEvent);
        }

        scheduleAt(nextEventTime, updateEvent);
        // Peter ev << "Start moving from "<< move.getStartPos().info() << " to "
        EV << "Start moving from "<< startPos << " to " // Added Peter
           // Peter << targetPos.info() << " in " << numSteps << " steps "
           << targetPos << " in " << numSteps << " steps " // Added Peter
           << " at speed=" << speed << endl;
    }
}

Coord TraceMobility::getLastPosition(){
    return lastPosition;
}

void TraceMobility::initializeTrace(const waypointEventsList* el)
{
    Enter_Method_Silent();
    if (el == NULL)
        return;
    // Copy the given event list to the internal list.
    eventList.resize(el->size());
    std::copy(el->begin(), el->end(), eventList.begin());
    scheduleNextWaypoint();
}
