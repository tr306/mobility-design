//
// Generated file, do not edit! Created by nedtool 5.6 from TraceMobilityProjectTemplate/TraceEv.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wshadow"
#  pragma clang diagnostic ignored "-Wconversion"
#  pragma clang diagnostic ignored "-Wunused-parameter"
#  pragma clang diagnostic ignored "-Wc++98-compat"
#  pragma clang diagnostic ignored "-Wunreachable-code-break"
#  pragma clang diagnostic ignored "-Wold-style-cast"
#elif defined(__GNUC__)
#  pragma GCC diagnostic ignored "-Wshadow"
#  pragma GCC diagnostic ignored "-Wconversion"
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  pragma GCC diagnostic ignored "-Wold-style-cast"
#  pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn"
#  pragma GCC diagnostic ignored "-Wfloat-conversion"
#endif

#include <iostream>
#include <sstream>
#include <memory>
#include "TraceEv_m.h"

namespace omnetpp {

// Template pack/unpack rules. They are declared *after* a1l type-specific pack functions for multiple reasons.
// They are in the omnetpp namespace, to allow them to be found by argument-dependent lookup via the cCommBuffer argument

// Packing/unpacking an std::vector
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::vector<T,A>& v)
{
    int n = v.size();
    doParsimPacking(buffer, n);
    for (int i = 0; i < n; i++)
        doParsimPacking(buffer, v[i]);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::vector<T,A>& v)
{
    int n;
    doParsimUnpacking(buffer, n);
    v.resize(n);
    for (int i = 0; i < n; i++)
        doParsimUnpacking(buffer, v[i]);
}

// Packing/unpacking an std::list
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::list<T,A>& l)
{
    doParsimPacking(buffer, (int)l.size());
    for (typename std::list<T,A>::const_iterator it = l.begin(); it != l.end(); ++it)
        doParsimPacking(buffer, (T&)*it);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::list<T,A>& l)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        l.push_back(T());
        doParsimUnpacking(buffer, l.back());
    }
}

// Packing/unpacking an std::set
template<typename T, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::set<T,Tr,A>& s)
{
    doParsimPacking(buffer, (int)s.size());
    for (typename std::set<T,Tr,A>::const_iterator it = s.begin(); it != s.end(); ++it)
        doParsimPacking(buffer, *it);
}

template<typename T, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::set<T,Tr,A>& s)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        T x;
        doParsimUnpacking(buffer, x);
        s.insert(x);
    }
}

// Packing/unpacking an std::map
template<typename K, typename V, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::map<K,V,Tr,A>& m)
{
    doParsimPacking(buffer, (int)m.size());
    for (typename std::map<K,V,Tr,A>::const_iterator it = m.begin(); it != m.end(); ++it) {
        doParsimPacking(buffer, it->first);
        doParsimPacking(buffer, it->second);
    }
}

template<typename K, typename V, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::map<K,V,Tr,A>& m)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        K k; V v;
        doParsimUnpacking(buffer, k);
        doParsimUnpacking(buffer, v);
        m[k] = v;
    }
}

// Default pack/unpack function for arrays
template<typename T>
void doParsimArrayPacking(omnetpp::cCommBuffer *b, const T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimPacking(b, t[i]);
}

template<typename T>
void doParsimArrayUnpacking(omnetpp::cCommBuffer *b, T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimUnpacking(b, t[i]);
}

// Default rule to prevent compiler from choosing base class' doParsimPacking() function
template<typename T>
void doParsimPacking(omnetpp::cCommBuffer *, const T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimPacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

template<typename T>
void doParsimUnpacking(omnetpp::cCommBuffer *, T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimUnpacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

}  // namespace omnetpp

namespace {
template <class T> inline
typename std::enable_if<std::is_polymorphic<T>::value && std::is_base_of<omnetpp::cObject,T>::value, void *>::type
toVoidPtr(T* t)
{
    return (void *)(static_cast<const omnetpp::cObject *>(t));
}

template <class T> inline
typename std::enable_if<std::is_polymorphic<T>::value && !std::is_base_of<omnetpp::cObject,T>::value, void *>::type
toVoidPtr(T* t)
{
    return (void *)dynamic_cast<const void *>(t);
}

template <class T> inline
typename std::enable_if<!std::is_polymorphic<T>::value, void *>::type
toVoidPtr(T* t)
{
    return (void *)static_cast<const void *>(t);
}

}


// forward
template<typename T, typename A>
std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec);

// Template rule to generate operator<< for shared_ptr<T>
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const std::shared_ptr<T>& t) { return out << t.get(); }

// Template rule which fires if a struct or class doesn't have operator<<
template<typename T>
inline std::ostream& operator<<(std::ostream& out,const T&) {return out;}

// operator<< for std::vector<T>
template<typename T, typename A>
inline std::ostream& operator<<(std::ostream& out, const std::vector<T,A>& vec)
{
    out.put('{');
    for(typename std::vector<T,A>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    {
        if (it != vec.begin()) {
            out.put(','); out.put(' ');
        }
        out << *it;
    }
    out.put('}');

    char buf[32];
    sprintf(buf, " (size=%u)", (unsigned int)vec.size());
    out.write(buf, strlen(buf));
    return out;
}

Register_Class(TraceEv)

TraceEv::TraceEv(const char *name, short kind) : ::omnetpp::cMessage(name, kind)
{
}

TraceEv::TraceEv(const TraceEv& other) : ::omnetpp::cMessage(other)
{
    copy(other);
}

TraceEv::~TraceEv()
{
}

TraceEv& TraceEv::operator=(const TraceEv& other)
{
    if (this == &other) return *this;
    ::omnetpp::cMessage::operator=(other);
    copy(other);
    return *this;
}

void TraceEv::copy(const TraceEv& other)
{
    this->time = other.time;
    this->nodeID = other.nodeID;
}

void TraceEv::parsimPack(omnetpp::cCommBuffer *b) const
{
    ::omnetpp::cMessage::parsimPack(b);
    doParsimPacking(b,this->time);
    doParsimPacking(b,this->nodeID);
}

void TraceEv::parsimUnpack(omnetpp::cCommBuffer *b)
{
    ::omnetpp::cMessage::parsimUnpack(b);
    doParsimUnpacking(b,this->time);
    doParsimUnpacking(b,this->nodeID);
}

omnetpp::simtime_t TraceEv::getTime() const
{
    return this->time;
}

void TraceEv::setTime(omnetpp::simtime_t time)
{
    this->time = time;
}

int TraceEv::getNodeID() const
{
    return this->nodeID;
}

void TraceEv::setNodeID(int nodeID)
{
    this->nodeID = nodeID;
}

class TraceEvDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertynames;
    enum FieldConstants {
        FIELD_time,
        FIELD_nodeID,
    };
  public:
    TraceEvDescriptor();
    virtual ~TraceEvDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyname) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyname) const override;
    virtual int getFieldArraySize(void *object, int field) const override;

    virtual const char *getFieldDynamicTypeString(void *object, int field, int i) const override;
    virtual std::string getFieldValueAsString(void *object, int field, int i) const override;
    virtual bool setFieldValueAsString(void *object, int field, int i, const char *value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual void *getFieldStructValuePointer(void *object, int field, int i) const override;
};

Register_ClassDescriptor(TraceEvDescriptor)

TraceEvDescriptor::TraceEvDescriptor() : omnetpp::cClassDescriptor(omnetpp::opp_typename(typeid(TraceEv)), "omnetpp::cMessage")
{
    propertynames = nullptr;
}

TraceEvDescriptor::~TraceEvDescriptor()
{
    delete[] propertynames;
}

bool TraceEvDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<TraceEv *>(obj)!=nullptr;
}

const char **TraceEvDescriptor::getPropertyNames() const
{
    if (!propertynames) {
        static const char *names[] = {  nullptr };
        omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
        const char **basenames = basedesc ? basedesc->getPropertyNames() : nullptr;
        propertynames = mergeLists(basenames, names);
    }
    return propertynames;
}

const char *TraceEvDescriptor::getProperty(const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : nullptr;
}

int TraceEvDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 2+basedesc->getFieldCount() : 2;
}

unsigned int TraceEvDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeFlags(field);
        field -= basedesc->getFieldCount();
    }
    static unsigned int fieldTypeFlags[] = {
        0,    // FIELD_time
        FD_ISEDITABLE,    // FIELD_nodeID
    };
    return (field >= 0 && field < 2) ? fieldTypeFlags[field] : 0;
}

const char *TraceEvDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldName(field);
        field -= basedesc->getFieldCount();
    }
    static const char *fieldNames[] = {
        "time",
        "nodeID",
    };
    return (field >= 0 && field < 2) ? fieldNames[field] : nullptr;
}

int TraceEvDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount() : 0;
    if (fieldName[0] == 't' && strcmp(fieldName, "time") == 0) return base+0;
    if (fieldName[0] == 'n' && strcmp(fieldName, "nodeID") == 0) return base+1;
    return basedesc ? basedesc->findField(fieldName) : -1;
}

const char *TraceEvDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeString(field);
        field -= basedesc->getFieldCount();
    }
    static const char *fieldTypeStrings[] = {
        "omnetpp::simtime_t",    // FIELD_time
        "int",    // FIELD_nodeID
    };
    return (field >= 0 && field < 2) ? fieldTypeStrings[field] : nullptr;
}

const char **TraceEvDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldPropertyNames(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

const char *TraceEvDescriptor::getFieldProperty(int field, const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldProperty(field, propertyname);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

int TraceEvDescriptor::getFieldArraySize(void *object, int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldArraySize(object, field);
        field -= basedesc->getFieldCount();
    }
    TraceEv *pp = (TraceEv *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

const char *TraceEvDescriptor::getFieldDynamicTypeString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldDynamicTypeString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    TraceEv *pp = (TraceEv *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string TraceEvDescriptor::getFieldValueAsString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldValueAsString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    TraceEv *pp = (TraceEv *)object; (void)pp;
    switch (field) {
        case FIELD_time: return simtime2string(pp->getTime());
        case FIELD_nodeID: return long2string(pp->getNodeID());
        default: return "";
    }
}

bool TraceEvDescriptor::setFieldValueAsString(void *object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->setFieldValueAsString(object,field,i,value);
        field -= basedesc->getFieldCount();
    }
    TraceEv *pp = (TraceEv *)object; (void)pp;
    switch (field) {
        case FIELD_nodeID: pp->setNodeID(string2long(value)); return true;
        default: return false;
    }
}

const char *TraceEvDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructName(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    };
}

void *TraceEvDescriptor::getFieldStructValuePointer(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructValuePointer(object, field, i);
        field -= basedesc->getFieldCount();
    }
    TraceEv *pp = (TraceEv *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

Register_Class(CreateEv)

CreateEv::CreateEv(const char *name, short kind) : ::TraceEv(name, kind)
{
}

CreateEv::CreateEv(const CreateEv& other) : ::TraceEv(other)
{
    copy(other);
}

CreateEv::~CreateEv()
{
}

CreateEv& CreateEv::operator=(const CreateEv& other)
{
    if (this == &other) return *this;
    ::TraceEv::operator=(other);
    copy(other);
    return *this;
}

void CreateEv::copy(const CreateEv& other)
{
    this->x = other.x;
    this->y = other.y;
    this->z = other.z;
    this->type = other.type;
    this->name = other.name;
}

void CreateEv::parsimPack(omnetpp::cCommBuffer *b) const
{
    ::TraceEv::parsimPack(b);
    doParsimPacking(b,this->x);
    doParsimPacking(b,this->y);
    doParsimPacking(b,this->z);
    doParsimPacking(b,this->type);
    doParsimPacking(b,this->name);
}

void CreateEv::parsimUnpack(omnetpp::cCommBuffer *b)
{
    ::TraceEv::parsimUnpack(b);
    doParsimUnpacking(b,this->x);
    doParsimUnpacking(b,this->y);
    doParsimUnpacking(b,this->z);
    doParsimUnpacking(b,this->type);
    doParsimUnpacking(b,this->name);
}

double CreateEv::getX() const
{
    return this->x;
}

void CreateEv::setX(double x)
{
    this->x = x;
}

double CreateEv::getY() const
{
    return this->y;
}

void CreateEv::setY(double y)
{
    this->y = y;
}

double CreateEv::getZ() const
{
    return this->z;
}

void CreateEv::setZ(double z)
{
    this->z = z;
}

const char * CreateEv::getType() const
{
    return this->type.c_str();
}

void CreateEv::setType(const char * type)
{
    this->type = type;
}

const char * CreateEv::getName() const
{
    return this->name.c_str();
}

void CreateEv::setName(const char * name)
{
    this->name = name;
}

class CreateEvDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertynames;
    enum FieldConstants {
        FIELD_x,
        FIELD_y,
        FIELD_z,
        FIELD_type,
        FIELD_name,
    };
  public:
    CreateEvDescriptor();
    virtual ~CreateEvDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyname) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyname) const override;
    virtual int getFieldArraySize(void *object, int field) const override;

    virtual const char *getFieldDynamicTypeString(void *object, int field, int i) const override;
    virtual std::string getFieldValueAsString(void *object, int field, int i) const override;
    virtual bool setFieldValueAsString(void *object, int field, int i, const char *value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual void *getFieldStructValuePointer(void *object, int field, int i) const override;
};

Register_ClassDescriptor(CreateEvDescriptor)

CreateEvDescriptor::CreateEvDescriptor() : omnetpp::cClassDescriptor(omnetpp::opp_typename(typeid(CreateEv)), "TraceEv")
{
    propertynames = nullptr;
}

CreateEvDescriptor::~CreateEvDescriptor()
{
    delete[] propertynames;
}

bool CreateEvDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<CreateEv *>(obj)!=nullptr;
}

const char **CreateEvDescriptor::getPropertyNames() const
{
    if (!propertynames) {
        static const char *names[] = {  nullptr };
        omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
        const char **basenames = basedesc ? basedesc->getPropertyNames() : nullptr;
        propertynames = mergeLists(basenames, names);
    }
    return propertynames;
}

const char *CreateEvDescriptor::getProperty(const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : nullptr;
}

int CreateEvDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 5+basedesc->getFieldCount() : 5;
}

unsigned int CreateEvDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeFlags(field);
        field -= basedesc->getFieldCount();
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,    // FIELD_x
        FD_ISEDITABLE,    // FIELD_y
        FD_ISEDITABLE,    // FIELD_z
        FD_ISEDITABLE,    // FIELD_type
        FD_ISEDITABLE,    // FIELD_name
    };
    return (field >= 0 && field < 5) ? fieldTypeFlags[field] : 0;
}

const char *CreateEvDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldName(field);
        field -= basedesc->getFieldCount();
    }
    static const char *fieldNames[] = {
        "x",
        "y",
        "z",
        "type",
        "name",
    };
    return (field >= 0 && field < 5) ? fieldNames[field] : nullptr;
}

int CreateEvDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount() : 0;
    if (fieldName[0] == 'x' && strcmp(fieldName, "x") == 0) return base+0;
    if (fieldName[0] == 'y' && strcmp(fieldName, "y") == 0) return base+1;
    if (fieldName[0] == 'z' && strcmp(fieldName, "z") == 0) return base+2;
    if (fieldName[0] == 't' && strcmp(fieldName, "type") == 0) return base+3;
    if (fieldName[0] == 'n' && strcmp(fieldName, "name") == 0) return base+4;
    return basedesc ? basedesc->findField(fieldName) : -1;
}

const char *CreateEvDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeString(field);
        field -= basedesc->getFieldCount();
    }
    static const char *fieldTypeStrings[] = {
        "double",    // FIELD_x
        "double",    // FIELD_y
        "double",    // FIELD_z
        "string",    // FIELD_type
        "string",    // FIELD_name
    };
    return (field >= 0 && field < 5) ? fieldTypeStrings[field] : nullptr;
}

const char **CreateEvDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldPropertyNames(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

const char *CreateEvDescriptor::getFieldProperty(int field, const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldProperty(field, propertyname);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

int CreateEvDescriptor::getFieldArraySize(void *object, int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldArraySize(object, field);
        field -= basedesc->getFieldCount();
    }
    CreateEv *pp = (CreateEv *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

const char *CreateEvDescriptor::getFieldDynamicTypeString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldDynamicTypeString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    CreateEv *pp = (CreateEv *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string CreateEvDescriptor::getFieldValueAsString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldValueAsString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    CreateEv *pp = (CreateEv *)object; (void)pp;
    switch (field) {
        case FIELD_x: return double2string(pp->getX());
        case FIELD_y: return double2string(pp->getY());
        case FIELD_z: return double2string(pp->getZ());
        case FIELD_type: return oppstring2string(pp->getType());
        case FIELD_name: return oppstring2string(pp->getName());
        default: return "";
    }
}

bool CreateEvDescriptor::setFieldValueAsString(void *object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->setFieldValueAsString(object,field,i,value);
        field -= basedesc->getFieldCount();
    }
    CreateEv *pp = (CreateEv *)object; (void)pp;
    switch (field) {
        case FIELD_x: pp->setX(string2double(value)); return true;
        case FIELD_y: pp->setY(string2double(value)); return true;
        case FIELD_z: pp->setZ(string2double(value)); return true;
        case FIELD_type: pp->setType((value)); return true;
        case FIELD_name: pp->setName((value)); return true;
        default: return false;
    }
}

const char *CreateEvDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructName(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    };
}

void *CreateEvDescriptor::getFieldStructValuePointer(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructValuePointer(object, field, i);
        field -= basedesc->getFieldCount();
    }
    CreateEv *pp = (CreateEv *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

Register_Class(DestroyEv)

DestroyEv::DestroyEv(const char *name, short kind) : ::TraceEv(name, kind)
{
}

DestroyEv::DestroyEv(const DestroyEv& other) : ::TraceEv(other)
{
    copy(other);
}

DestroyEv::~DestroyEv()
{
}

DestroyEv& DestroyEv::operator=(const DestroyEv& other)
{
    if (this == &other) return *this;
    ::TraceEv::operator=(other);
    copy(other);
    return *this;
}

void DestroyEv::copy(const DestroyEv& other)
{
}

void DestroyEv::parsimPack(omnetpp::cCommBuffer *b) const
{
    ::TraceEv::parsimPack(b);
}

void DestroyEv::parsimUnpack(omnetpp::cCommBuffer *b)
{
    ::TraceEv::parsimUnpack(b);
}

class DestroyEvDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertynames;
    enum FieldConstants {
    };
  public:
    DestroyEvDescriptor();
    virtual ~DestroyEvDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyname) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyname) const override;
    virtual int getFieldArraySize(void *object, int field) const override;

    virtual const char *getFieldDynamicTypeString(void *object, int field, int i) const override;
    virtual std::string getFieldValueAsString(void *object, int field, int i) const override;
    virtual bool setFieldValueAsString(void *object, int field, int i, const char *value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual void *getFieldStructValuePointer(void *object, int field, int i) const override;
};

Register_ClassDescriptor(DestroyEvDescriptor)

DestroyEvDescriptor::DestroyEvDescriptor() : omnetpp::cClassDescriptor(omnetpp::opp_typename(typeid(DestroyEv)), "TraceEv")
{
    propertynames = nullptr;
}

DestroyEvDescriptor::~DestroyEvDescriptor()
{
    delete[] propertynames;
}

bool DestroyEvDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<DestroyEv *>(obj)!=nullptr;
}

const char **DestroyEvDescriptor::getPropertyNames() const
{
    if (!propertynames) {
        static const char *names[] = {  nullptr };
        omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
        const char **basenames = basedesc ? basedesc->getPropertyNames() : nullptr;
        propertynames = mergeLists(basenames, names);
    }
    return propertynames;
}

const char *DestroyEvDescriptor::getProperty(const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : nullptr;
}

int DestroyEvDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 0+basedesc->getFieldCount() : 0;
}

unsigned int DestroyEvDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeFlags(field);
        field -= basedesc->getFieldCount();
    }
    return 0;
}

const char *DestroyEvDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldName(field);
        field -= basedesc->getFieldCount();
    }
    return nullptr;
}

int DestroyEvDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->findField(fieldName) : -1;
}

const char *DestroyEvDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeString(field);
        field -= basedesc->getFieldCount();
    }
    return nullptr;
}

const char **DestroyEvDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldPropertyNames(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

const char *DestroyEvDescriptor::getFieldProperty(int field, const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldProperty(field, propertyname);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

int DestroyEvDescriptor::getFieldArraySize(void *object, int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldArraySize(object, field);
        field -= basedesc->getFieldCount();
    }
    DestroyEv *pp = (DestroyEv *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

const char *DestroyEvDescriptor::getFieldDynamicTypeString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldDynamicTypeString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    DestroyEv *pp = (DestroyEv *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string DestroyEvDescriptor::getFieldValueAsString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldValueAsString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    DestroyEv *pp = (DestroyEv *)object; (void)pp;
    switch (field) {
        default: return "";
    }
}

bool DestroyEvDescriptor::setFieldValueAsString(void *object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->setFieldValueAsString(object,field,i,value);
        field -= basedesc->getFieldCount();
    }
    DestroyEv *pp = (DestroyEv *)object; (void)pp;
    switch (field) {
        default: return false;
    }
}

const char *DestroyEvDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructName(field);
        field -= basedesc->getFieldCount();
    }
    return nullptr;
}

void *DestroyEvDescriptor::getFieldStructValuePointer(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructValuePointer(object, field, i);
        field -= basedesc->getFieldCount();
    }
    DestroyEv *pp = (DestroyEv *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

Register_Class(SetDestEv)

SetDestEv::SetDestEv(const char *name, short kind) : ::TraceEv(name, kind)
{
}

SetDestEv::SetDestEv(const SetDestEv& other) : ::TraceEv(other)
{
    copy(other);
}

SetDestEv::~SetDestEv()
{
}

SetDestEv& SetDestEv::operator=(const SetDestEv& other)
{
    if (this == &other) return *this;
    ::TraceEv::operator=(other);
    copy(other);
    return *this;
}

void SetDestEv::copy(const SetDestEv& other)
{
    this->x = other.x;
    this->y = other.y;
    this->z = other.z;
    this->speed = other.speed;
    this->timeAtDest = other.timeAtDest;
}

void SetDestEv::parsimPack(omnetpp::cCommBuffer *b) const
{
    ::TraceEv::parsimPack(b);
    doParsimPacking(b,this->x);
    doParsimPacking(b,this->y);
    doParsimPacking(b,this->z);
    doParsimPacking(b,this->speed);
    doParsimPacking(b,this->timeAtDest);
}

void SetDestEv::parsimUnpack(omnetpp::cCommBuffer *b)
{
    ::TraceEv::parsimUnpack(b);
    doParsimUnpacking(b,this->x);
    doParsimUnpacking(b,this->y);
    doParsimUnpacking(b,this->z);
    doParsimUnpacking(b,this->speed);
    doParsimUnpacking(b,this->timeAtDest);
}

double SetDestEv::getX() const
{
    return this->x;
}

void SetDestEv::setX(double x)
{
    this->x = x;
}

double SetDestEv::getY() const
{
    return this->y;
}

void SetDestEv::setY(double y)
{
    this->y = y;
}

double SetDestEv::getZ() const
{
    return this->z;
}

void SetDestEv::setZ(double z)
{
    this->z = z;
}

double SetDestEv::getSpeed() const
{
    return this->speed;
}

void SetDestEv::setSpeed(double speed)
{
    this->speed = speed;
}

omnetpp::simtime_t SetDestEv::getTimeAtDest() const
{
    return this->timeAtDest;
}

void SetDestEv::setTimeAtDest(omnetpp::simtime_t timeAtDest)
{
    this->timeAtDest = timeAtDest;
}

class SetDestEvDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertynames;
    enum FieldConstants {
        FIELD_x,
        FIELD_y,
        FIELD_z,
        FIELD_speed,
        FIELD_timeAtDest,
    };
  public:
    SetDestEvDescriptor();
    virtual ~SetDestEvDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyname) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyname) const override;
    virtual int getFieldArraySize(void *object, int field) const override;

    virtual const char *getFieldDynamicTypeString(void *object, int field, int i) const override;
    virtual std::string getFieldValueAsString(void *object, int field, int i) const override;
    virtual bool setFieldValueAsString(void *object, int field, int i, const char *value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual void *getFieldStructValuePointer(void *object, int field, int i) const override;
};

Register_ClassDescriptor(SetDestEvDescriptor)

SetDestEvDescriptor::SetDestEvDescriptor() : omnetpp::cClassDescriptor(omnetpp::opp_typename(typeid(SetDestEv)), "TraceEv")
{
    propertynames = nullptr;
}

SetDestEvDescriptor::~SetDestEvDescriptor()
{
    delete[] propertynames;
}

bool SetDestEvDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<SetDestEv *>(obj)!=nullptr;
}

const char **SetDestEvDescriptor::getPropertyNames() const
{
    if (!propertynames) {
        static const char *names[] = {  nullptr };
        omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
        const char **basenames = basedesc ? basedesc->getPropertyNames() : nullptr;
        propertynames = mergeLists(basenames, names);
    }
    return propertynames;
}

const char *SetDestEvDescriptor::getProperty(const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? basedesc->getProperty(propertyname) : nullptr;
}

int SetDestEvDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    return basedesc ? 5+basedesc->getFieldCount() : 5;
}

unsigned int SetDestEvDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeFlags(field);
        field -= basedesc->getFieldCount();
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,    // FIELD_x
        FD_ISEDITABLE,    // FIELD_y
        FD_ISEDITABLE,    // FIELD_z
        FD_ISEDITABLE,    // FIELD_speed
        0,    // FIELD_timeAtDest
    };
    return (field >= 0 && field < 5) ? fieldTypeFlags[field] : 0;
}

const char *SetDestEvDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldName(field);
        field -= basedesc->getFieldCount();
    }
    static const char *fieldNames[] = {
        "x",
        "y",
        "z",
        "speed",
        "timeAtDest",
    };
    return (field >= 0 && field < 5) ? fieldNames[field] : nullptr;
}

int SetDestEvDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    int base = basedesc ? basedesc->getFieldCount() : 0;
    if (fieldName[0] == 'x' && strcmp(fieldName, "x") == 0) return base+0;
    if (fieldName[0] == 'y' && strcmp(fieldName, "y") == 0) return base+1;
    if (fieldName[0] == 'z' && strcmp(fieldName, "z") == 0) return base+2;
    if (fieldName[0] == 's' && strcmp(fieldName, "speed") == 0) return base+3;
    if (fieldName[0] == 't' && strcmp(fieldName, "timeAtDest") == 0) return base+4;
    return basedesc ? basedesc->findField(fieldName) : -1;
}

const char *SetDestEvDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldTypeString(field);
        field -= basedesc->getFieldCount();
    }
    static const char *fieldTypeStrings[] = {
        "double",    // FIELD_x
        "double",    // FIELD_y
        "double",    // FIELD_z
        "double",    // FIELD_speed
        "omnetpp::simtime_t",    // FIELD_timeAtDest
    };
    return (field >= 0 && field < 5) ? fieldTypeStrings[field] : nullptr;
}

const char **SetDestEvDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldPropertyNames(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

const char *SetDestEvDescriptor::getFieldProperty(int field, const char *propertyname) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldProperty(field, propertyname);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

int SetDestEvDescriptor::getFieldArraySize(void *object, int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldArraySize(object, field);
        field -= basedesc->getFieldCount();
    }
    SetDestEv *pp = (SetDestEv *)object; (void)pp;
    switch (field) {
        default: return 0;
    }
}

const char *SetDestEvDescriptor::getFieldDynamicTypeString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldDynamicTypeString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    SetDestEv *pp = (SetDestEv *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string SetDestEvDescriptor::getFieldValueAsString(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldValueAsString(object,field,i);
        field -= basedesc->getFieldCount();
    }
    SetDestEv *pp = (SetDestEv *)object; (void)pp;
    switch (field) {
        case FIELD_x: return double2string(pp->getX());
        case FIELD_y: return double2string(pp->getY());
        case FIELD_z: return double2string(pp->getZ());
        case FIELD_speed: return double2string(pp->getSpeed());
        case FIELD_timeAtDest: return simtime2string(pp->getTimeAtDest());
        default: return "";
    }
}

bool SetDestEvDescriptor::setFieldValueAsString(void *object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->setFieldValueAsString(object,field,i,value);
        field -= basedesc->getFieldCount();
    }
    SetDestEv *pp = (SetDestEv *)object; (void)pp;
    switch (field) {
        case FIELD_x: pp->setX(string2double(value)); return true;
        case FIELD_y: pp->setY(string2double(value)); return true;
        case FIELD_z: pp->setZ(string2double(value)); return true;
        case FIELD_speed: pp->setSpeed(string2double(value)); return true;
        default: return false;
    }
}

const char *SetDestEvDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructName(field);
        field -= basedesc->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    };
}

void *SetDestEvDescriptor::getFieldStructValuePointer(void *object, int field, int i) const
{
    omnetpp::cClassDescriptor *basedesc = getBaseClassDescriptor();
    if (basedesc) {
        if (field < basedesc->getFieldCount())
            return basedesc->getFieldStructValuePointer(object, field, i);
        field -= basedesc->getFieldCount();
    }
    SetDestEv *pp = (SetDestEv *)object; (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

