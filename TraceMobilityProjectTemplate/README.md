# TraceMobilityProjectTemplate

3.6.2020
Change to TraceMobility.cc to make 3D movement possible
(To make nodes move 3D, also use SoftwareLabMobility.cc. Code can be tested with tracefile oneMovingNode.tr)

4.6.2020
Change to TemplateNetwork.ned for 3D movement
