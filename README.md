# Entwicklung eines Mobilitätsmodells für Autonome Unterwasserfahrzeuge (Design of a Mobility Model for Autonomous Underwater Vehicles) 

<p> Nach dem Zweiten Weltkrieg wurde viel Militärmunition in der Ostsee entsorgt. Die Gebiete mit Blindgängern (Unexploded Ordnance, UXO) sind grob bekannt; beispielsweise die Kolberger Heide nordöstlich von Kiel. Die genaue Position und Anzahl dieser alten Munition sind jedoch unbekannt. Auch ihr Zustand ist unbekannt, was diese Gebiete sehr gefährlich macht. Das Deutsche Zentrum für Luft- und Raumfahrt (DLR) in Bremerhaven plant, mehrere Autonome Unterwasserfahrzeuge (Autonomous Underwater Vehicles, AUVs) vom Typ Atlas Seacat zu verwenden, um Blindgänger zu suchen und zu lokalisieren, um sie unschädlich zu machen und ordnungsgemäß zu entsorgen. Diese AUVs sind mit einem Vermessungskopf ausgestattet, der aus mehreren Sensoren und einem akustischen Modem für die Kommunikation besteht und unterliegen Beschränkungen hinsichtlich der ihnen für einen Einsatz unter Wasser zur Verfügung stehenden Energie. Diese Arbeit zielt u.a. darauf ab, den Energieverbrauch der AUVs in einer 3D-Umgebung in dem Netzwerksimulator OMNeT++ zu simulieren, der sich vor allen Dingen durch die Bewegung aber auch durch die Sensorik und Kommunikation ergibt. Die bisher ausschließlich zentrale (d.h. statische) Planung der Bewegung soll autonom zur Laufzeit angepasst werden können. Es ist ein Mobilitätsmodell zu entwickeln, für das verschiedene Fahrmuster der AUVs so geplant und autonom angepasst werden können, dass der Energieverbrauch der AUVs so gering wie möglich ist, aber gleichzeitig z.B. eine Fläche in kürzester Zeit nach Blindgängern abgesucht werden kann. Die Bachelorarbeit wird in Zusammenarbeit mit dem DLR Bremerhaven durchgeführt. Zusammengefasst sind folgende Aufgaben zu lösen:  </p>

- Einarbeitung in die Grundlagen von AUVs <br>
- Recherche des Stands der Technik <br>
- Entwurf einer Architektur für ein AUV <br>
- Entwicklung eines Mobilitätsmodells mit Energieverbrauchsmessung zur zentralen Planung  und autonomen Anpassung von Fahrmustern in OMNeT++ <br>
- Simulation des Mobilitätsmodells in OMNeT++ <br>
- Umfassende qualitative und quantitative Bewertung der Simulationsergebnisse <br>

> Betreuender Hochschullehrer: Dr.-Ing. habil. Peter Danielis Betreuer:    Dr.-Ing. habil. > > Peter Danielis, Dr.-Ing. Helge Parzyjegla 

Startzeitpunkt:   9. November 2020 (20 Wochen) 

Inhalt der Branches: <br>
working_app_v1: Kurveninterpolation vom Nullpunkt aus in Richtung der y-Achse<br>
working_app_v2: Kurveninterpolation von belibigen Punkten mit belibigen Richtungen<br>
working_app_v3_without_output: Finale Version mit einem gefixten Bug.<br>
working_app_v3_with_output: Finlae Version mit der Wegpunktausgabe in einer CSV-Datei<br>

